from json import load as json_load

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
from plotly.subplots import make_subplots

file = "data.json"
try:
    with open(file, 'r') as file_data:
        data = json_load(file_data)
except:
    exit()

x = data['record']['dates']
y1 = [k[0] for k in data['record']['txrx']]
y2 = [k[1] for k in data['record']['txrx']]
y3 = data['record']['ping']


def update_graph():
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    # Add traces
    # fig.add_trace(go.Scatter(x=x, y=y1, name="Received"), secondary_y=False, )
    # fig.add_trace(go.Scatter(x=x, y=y2, name="Transmitted"), secondary_y=False, )
    fig.add_trace(go.Scatter(x=x, y=y3, name="Ping"), secondary_y=True)
    # Add figure title
    fig.update_layout(title_text="Monitoring network")
    # Set x-axis title
    fig.update_xaxes(title_text="Temps")
    # Set y-axes titles
    fig.update_yaxes(title_text="Quantité de données", secondary_y=False)
    fig.update_yaxes(title_text="Ping (ms)", secondary_y=True)
    return fig


app = dash.Dash(__name__)

styles = {
    'pre': {
        'border': 'thin lightgrey solid',
        'overflowX': 'scroll'
    }
}

app.layout = html.Div([
    dcc.Graph(figure=update_graph())
])

if __name__ == '__main__':
    app.run_server()
