import argparse
import datetime
import re
import subprocess
from json import dump as json_dump
from json import load as json_load

parser = argparse.ArgumentParser()
parser.add_argument("--interface", help="Network interface to monitor", default='eth0', type=str)
parser.add_argument("--file", help="File where to save data", default='data.json', type=str)
args = vars(parser.parse_args())
file = args['file']
network_interface = args['interface']

cmds = {'txrx':
            {'cmd': 'ip -s link',
             'regex': re.compile(
                 r"%interface%: <.*> .*\n.*\n.*\n {4}([0-9]*).*\n.*\n {4}([0-9]*)".replace('%interface%',
                                                                                           network_interface),
                 re.MULTILINE),
             'groups': [0, 1],
             'parse': int
             },
        'ping':
            {'cmd': 'ping -c 5 -I %interface% 1.1.1.1'.replace('%interface%', network_interface),
             'regex': re.compile(r"rtt min/avg/max/mdev = ([0-9]|.*)\/([0-9]|.*)\/([0-9]|.*)\/([0-9]|.*) ms"),
             'groups': [1],
             'parse': lambda x: int(float(x))
             }
        }


def get_data(cmd, regex, groups, parse):
    result = subprocess.getoutput(cmd)
    s = re.search(regex, result)
    if len(groups) > 1:
        retrn = []
        for k in groups:
            retrn.append(parse(s.groups()[k]))
        return retrn
    else:
        return parse(s.groups()[groups[0]])


try:
    with open(file, 'r') as file_data:
        data = json_load(file_data)
    last_data = data['record']
except:
    data = {'record': {'dates': []}}
    for k in cmds:
        data['record'][k] = []

data['record']['dates'].append(str(datetime.datetime.now()))

for k, value in cmds.items():
    data['record'][k].append(get_data(**value))

with open(file, 'w') as file_data:
    json_dump(data, file_data, indent=2)
